#ifndef LED_CONTROL
#define LED_CONTROL

#define RED_LED_PIN 11
#define GREEN_LED_PIN 10
#define ORANGE_LED_PIN 12

bool redLED = false;
bool greenLED = false;
bool orangeLED = false;
bool flashOrange = true;

void updateLEDs();
void wait(int time);

#endif
