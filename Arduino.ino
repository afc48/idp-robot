#include "MotorControl.h"
#include "SensorControl.h"
#include "SharpIR.h"
#include "RobotLogic.h"
#include "ledControl.h"

void setup()
{
  initLEDs();
  initMotors();
  initSensors();
  
  Serial.begin(9600);
  Serial.write("Starting...\n");

  wait(50);

  greenLED = true;
  redLED = true;
  orangeLED = true;
  flashOrange = false;

  // Wait for the button to be pushed
  while (digitalRead(START_BUTTON_PIN) == LOW)
  {
    wait(20);
  }

  greenLED = false;
  redLED = false;
  flashOrange = true;

  wait(1000);
}

void loop()
{
  followLine(); // Perform line following logic code
  updateState(); // Check if we are at a junction and update the state variable appropriately

  // Check if there is a fruit in close proximity
  if (distanceSensor.distance() < 8 && state == STATE_MAIN_LOOP)
  {
    processFruit();
  }
  
  wait(20);
}
