#ifndef ROBOT_LOGIC
#define ROBOT_LOGIC

#define STATE_START_BOX      0
#define STATE_START_TRACK    1
#define STATE_MAIN_LOOP      2
#define STATE_END_TRACK      3

int state = STATE_START_BOX;
unsigned long timeOfLastStateChange = 0;

void avoidFruit();
void processFruit();
bool waitUntilLineOrTime(int time);

void followLine();

void updateState();

#endif
