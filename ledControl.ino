#include "ledControl.h"

void initLEDs()
{
  pinMode(ORANGE_LED_PIN, OUTPUT);
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(GREEN_LED_PIN, OUTPUT);
}

void updateLEDs()
{
  digitalWrite(RED_LED_PIN, redLED);
  digitalWrite(GREEN_LED_PIN, greenLED);

  // Flash orange LED at 2 Hz
  if (((int)(millis() / 250) % 2 == 0) && flashOrange)
  {
    digitalWrite(ORANGE_LED_PIN, 0);
  }
  else
  {
    digitalWrite(ORANGE_LED_PIN, orangeLED);
  }
}

void wait(int time)
{
  unsigned long startWait = millis();

  // Loop until the required time has elapsed
  while (millis() < (startWait + time))
  {
    // Keep continuously updating the LEDs every time wait is called
    updateLEDs();
    delay(1);
  }
}
