#include <Wire.h>
#include <Servo.h>
#include <Adafruit_MotorShield.h>
#include "MotorControl.h"
#include "ledControl.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
Adafruit_DCMotor *leftMotor = AFMS.getMotor(2);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(1);

Servo robotArm;

void initMotors()
{
  AFMS.begin();
  
  setLeftSpeed(0);
  setRightSpeed(0);

  robotArm.attach(9); // The servo is attached to pin 9
  robotArm.writeMicroseconds(ROBOT_ARM_DOWN_POSITION);
}

void setLeftSpeed(int targetSpeed)
{
  // Set the magnitude of the speed
  leftMotor->setSpeed(abs(targetSpeed));

  // Set the motor direction
  if (targetSpeed > 0)
  {
    leftMotor->run(BACKWARD);
  }
  else if (targetSpeed < 0)
  {
    leftMotor->run(FORWARD);
  }
  else
  {
    leftMotor->run(RELEASE);
  }

  leftMotorSpeed = targetSpeed;
}

void setRightSpeed(int targetSpeed)
{
  // Set the magnitude of the speed
  rightMotor->setSpeed(abs(targetSpeed));

  // Set the motor direction
  if (targetSpeed > 0)
  {
    rightMotor->run(BACKWARD);
  }
  else if (targetSpeed < 0)
  {
    rightMotor->run(FORWARD);
  }
  else
  {
    rightMotor->run(RELEASE);
  }

  rightMotorSpeed = targetSpeed;
}

int lastForwardsMove = 0; // 0 = left, 1 = right
unsigned long timeOfChangeOfDirection = 0;
int waitBeforeReverseWheel = 200; // How much time to wait before increasing the rate of turn

void goForwardsLeft()
{
  unsigned long time = millis();

  // If this is a change from last time
  if (lastForwardsMove == 1)
  {
    lastForwardsMove = 0;
    timeOfChangeOfDirection = time;
  }

  // If we have been trying to turn for a certain time, we make the turn sharper by setting one wheel to reverse
  if ((time - timeOfChangeOfDirection) < waitBeforeReverseWheel)
  {
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED * 0.85);
    setLeftSpeed(0);
  }
  else
  {
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED * 0.85);
    setLeftSpeed(-LEFT_MOTOR_FULL_SPEED * 0.85);
  }
}

void goForwardsRight()
{
  unsigned long time = millis();

  // If this is a change from last time
  if (lastForwardsMove == 0)
  {
    lastForwardsMove = 1;
    timeOfChangeOfDirection = time;
  }

  // If we have been trying to turn for a certain time, we make the turn sharper by setting one wheel to reverse
  if ((time - timeOfChangeOfDirection) < waitBeforeReverseWheel)
  {
    setRightSpeed(0);
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED * 0.85);
  }
  else
  {
    setRightSpeed(-RIGHT_MOTOR_FULL_SPEED * 0.85);
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED * 0.85);
  }
}

void openRobotArm()
{
  for (int i = ROBOT_ARM_DOWN_POSITION; i > ROBOT_ARM_UP_POSITION; i--)
  {
    robotArm.writeMicroseconds(i);
    wait(10);
  }
}

void closeRobotArm()
{
  for (int i = ROBOT_ARM_UP_POSITION; i < ROBOT_ARM_DOWN_POSITION; i++)
  {
    robotArm.writeMicroseconds(i);
    wait(10);
  }
}
