#ifndef MOTOR_CONTROL
#define MOTOR_CONTROL

#define ROBOT_ARM_UP_POSITION 1400
#define ROBOT_ARM_DOWN_POSITION 1630 // Was 1550

int LEFT_MOTOR_FULL_SPEED = 255;
int RIGHT_MOTOR_FULL_SPEED = 220;

uint8_t leftMotorSpeed = 0;
uint8_t rightMotorSpeed = 0;

void initMotors();

void setLeftSpeed(int targetSpeed);
void setRightSpeed(int targetSpeed);

void goForwardsLeft();
void goForwardsRight();

void openRobotArm();
void closeRobotArm();

#endif
