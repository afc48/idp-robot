#include "RobotLogic.h"
#include "SensorControl.h"
#include "MotorControl.h"
#include "ledControl.h"

int fruitStepCount = 0;

void repositionRobotForFruit()
{
  // Move back to the original position before we moved backwards/forwards for colour detection
  if (fruitStepCount > 0)
  {
    setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
    wait(20 * fruitStepCount);
  }
  else if (fruitStepCount < 0)
  {
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
    wait(20 * abs(fruitStepCount));
  }

  setLeftSpeed(0);
  setRightSpeed(0);
  fruitStepCount = 0;
}

void processFruit()
{
  setLeftSpeed(0);
  setRightSpeed(0);
  flashOrange = false;
  wait(1500);
  updateSensorInfo();
  flashOrange = true;

  // Dectecting red: Open arm and move forwards, then close arm
  if (detectingRed && !detectingBlue)
  {
    repositionRobotForFruit();
    greenLED = true;
    wait(3000);
    openRobotArm();
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
    wait(1000);
    setLeftSpeed(0);
    setRightSpeed(0);
    closeRobotArm();
    greenLED = false;
  }
  // Detecting blue: call avoidFruit()
  else if (detectingBlue && !detectingRed)
  {
    repositionRobotForFruit();
    redLED = true;
    wait(2000);
    avoidFruit();
    redLED = false;
  }
  // Detecting red and blue: Move a step back and test the colours again
  else if (detectingRed && detectingBlue)
  {
    setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(-RIGHT_MOTOR_FULL_SPEED*0.8); // *0.8 to account for the different angular accelerations of the motors
    wait(30);
    setLeftSpeed(0);
    setRightSpeed(0);
    processFruit();
    fruitStepCount--;
  }
  // Detecting no colour: Move closer to get a better view and test the colours again
  else
  {
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED*0.8); // *0.8 to account for the different angular accelerations of the motors
    wait(30);
    setLeftSpeed(0);
    setRightSpeed(0);
    processFruit();
    fruitStepCount++;
  }
}

void avoidFruit()
{
  int distanceTime = 1500;
  int angleTime = 1250;
  
  setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
  wait(300);

  setLeftSpeed(0);
  setRightSpeed(0);
  wait(500);

  setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
  wait(angleTime);

  setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
  wait(distanceTime);

  setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
  wait(angleTime);

  setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
  if (!waitUntilLineOrTime(distanceTime*2)) // didnt hit line, so continue
  {
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
    wait(angleTime);
  
    setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
    setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
    if(waitUntilLineOrTime(distanceTime*1.5)) // hit line early, so turn and resume line following
    {
      setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
      setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
      wait(150);
      setLeftSpeed(0);
      setRightSpeed(0);
    }
  }
  else // hit line early, so resume line following
  {
      setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
      setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
      wait(150);
      setLeftSpeed(0);
      setRightSpeed(0);
  }

  setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
  setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
  wait(angleTime);

  setLeftSpeed(0);
  setRightSpeed(0);
}

bool waitUntilLineOrTime(int time)
{
  unsigned long startTime = millis();

  // Loop until the required time has elapsed
  while (time > (millis() - startTime))
  {
    // But if we have hit the line, return early
    updateSensorInfo();
    if ((valA0 + valA1) > 400)
    {
      return true;
    }
    
    wait(20);
  }

  return false;
}

void followLine()
{
  updateSensorInfo();

  // If we are not on the line, turn left since the left turns are the sharpest turns
  // where we are most likely to lose the line
  if ((valA0 + valA1) < 100)
  {
    goForwardsLeft();
  }
  // If the left sensor is brighter than the right one, turn left
  else if (valA0 < valA1)
  {
    goForwardsLeft();
    //Serial.write("Left..\n");
  }
  // Otherwise turn right
  else
  {
    goForwardsRight();
    //Serial.write("Right...\n");
  }
}

void updateState()
{
  updateSensorInfo();

  // If our junction line sensor has activated and there has been enough time since we left
  // the last junction (to avoid a single junction causing us to advance the state variable
  // multiple times)
  if (valA3 > 100 && (millis() - timeOfLastStateChange) > 3000) // Hit junction!
  {
    timeOfLastStateChange = millis();

    switch (state) // Switch through the state we are about to leave
    {
      case STATE_START_BOX: // Skip over the box edges and resume
      
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
        wait(300);
        setLeftSpeed(0);
        setRightSpeed(0);

      break;
      case STATE_START_TRACK: // Turn by 90 degrees to the right and proceed
      
        setLeftSpeed(0);
        setRightSpeed(0);
        wait(200);
        
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
        wait(150);
    
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
        wait(1250);
    
        setLeftSpeed(0);
        setRightSpeed(0);

      break;
      case STATE_MAIN_LOOP: // Turn right by 90 degrees and proceed

        setLeftSpeed(0);
        setRightSpeed(0);
        wait(200);
        
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
        wait(150);
    
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
        wait(1250);
    
        setLeftSpeed(0);
        setRightSpeed(0);

      break;
      case STATE_END_TRACK: // Move forwards, open arm, move back, close arm and return home

        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(RIGHT_MOTOR_FULL_SPEED);
        wait(300);
        setLeftSpeed(0);
        setRightSpeed(0);
        openRobotArm();

        setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
        wait(1000);
        setLeftSpeed(0);
        setRightSpeed(0);
        closeRobotArm();
        
        setLeftSpeed(LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
        wait(175);
        
        setLeftSpeed(-LEFT_MOTOR_FULL_SPEED);
        setRightSpeed(-RIGHT_MOTOR_FULL_SPEED);
        wait(9250);
        setLeftSpeed(0);
        setRightSpeed(0);     

        orangeLED = false;

        // Hang indefinitely
        while (1)
        {
          wait(1000);
        }

      break;
      default:
      break;
    }

    state++;
  }
}
