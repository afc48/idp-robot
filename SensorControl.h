#ifndef SENSOR_CONTROL
#define SENSOR_CONTROL

#include "SharpIR.h"

#define START_BUTTON_PIN 6

int valA0, valA1, valA3;
bool detectingRed, detectingBlue;

SharpIR distanceSensor = SharpIR(A2, 1080);

void initSensors();
void printSensorInfo();
void updateSensorInfo();

#endif
