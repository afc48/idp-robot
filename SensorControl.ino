#include "SensorControl.h"
#include "ledControl.h"

void initSensors()
{
  pinMode(A0, INPUT); // Line sensor
  pinMode(A1, INPUT); // Line sensor
  pinMode(A3, INPUT); // Line sensor
  
  pinMode(A2, INPUT); // Distance sensor

  pinMode(2, INPUT); // Red colour sensor
  pinMode(3, INPUT); // Blue colour sensor

  // Start button
  pinMode(6, INPUT);

  // Put a voltage over the button to detect when it is pressed
  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH);

  updateSensorInfo();
}

void updateSensorInfo()
{
  valA0 = 0;
  valA1 = 0;
  valA3 = 0;

  // Read 10 times to reduce the impacts of noise
  for (int i = 0; i < 10; i++)
  {
    valA0 += analogRead(A0);
    valA1 += analogRead(A1);
    valA3 += analogRead(A3);
  }

  detectingRed = digitalRead(2);
  detectingBlue = digitalRead(3);
}

void printSensorInfo()
{
    Serial.write("A0: ");
    Serial.write(String(valA0).c_str());
  
    Serial.write(", A1: ");
    Serial.write(String(valA1).c_str());
  
    Serial.write(", A2: ");
    Serial.write(String(valA2).c_str());
  
    Serial.write(", A3: ");
    Serial.write(String(valA3).c_str());

    Serial.write("\n");
}
